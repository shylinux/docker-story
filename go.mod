module shylinux.com/x/docker-story

go 1.13

replace (
	shylinux.com/x/ice => ./usr/release
	shylinux.com/x/icebergs => ./usr/icebergs
	shylinux.com/x/toolkits => ./usr/toolkits
)

require (
	shylinux.com/x/ice v1.3.13
	shylinux.com/x/icebergs v1.5.19
	shylinux.com/x/toolkits v0.7.10
)

require (
	github.com/shirou/gopsutil/v3 v3.23.7 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
