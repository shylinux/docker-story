package main

import (
	"shylinux.com/x/ice"

	_ "shylinux.com/x/docker-story/src/client"
	_ "shylinux.com/x/docker-story/src/server"
	_ "shylinux.com/x/docker-story/src/service/coder"
	_ "shylinux.com/x/docker-story/src/service/consul"
	_ "shylinux.com/x/docker-story/src/service/drawio"
	_ "shylinux.com/x/docker-story/src/service/elasticsearch"
	_ "shylinux.com/x/docker-story/src/service/ewomail"
	_ "shylinux.com/x/docker-story/src/service/gitea"
	_ "shylinux.com/x/docker-story/src/service/grafana"
	_ "shylinux.com/x/docker-story/src/service/jenkins"
	_ "shylinux.com/x/docker-story/src/service/kafka"
	_ "shylinux.com/x/docker-story/src/service/mariadb"
	_ "shylinux.com/x/docker-story/src/service/mediawiki"
	_ "shylinux.com/x/docker-story/src/service/minio"
	_ "shylinux.com/x/docker-story/src/service/mongo"
	_ "shylinux.com/x/docker-story/src/service/mysql"
	_ "shylinux.com/x/docker-story/src/service/nextcloud"
	_ "shylinux.com/x/docker-story/src/service/nginx"
	_ "shylinux.com/x/docker-story/src/service/onlyoffice"
	_ "shylinux.com/x/docker-story/src/service/portainer"
	_ "shylinux.com/x/docker-story/src/service/postgres"
	_ "shylinux.com/x/docker-story/src/service/prometheus"
	_ "shylinux.com/x/docker-story/src/service/pulsar"
	_ "shylinux.com/x/docker-story/src/service/redis"
	_ "shylinux.com/x/docker-story/src/service/registry"
	_ "shylinux.com/x/docker-story/src/service/rocketchat"
	_ "shylinux.com/x/docker-story/src/service/zookeeper"
	_ "shylinux.com/x/docker-story/src/swarm"
	_ "shylinux.com/x/docker-story/src/yaml"
)

func main() { print(ice.Run()) }
