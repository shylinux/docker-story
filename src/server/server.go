package server

import "shylinux.com/x/ice"

type server struct {
	ice.Code
	darwin string `data:"https://download.docker.com/mac/static/stable/x86_64/docker-19.03.9.tgz"`
	linux  string `data:"https://download.docker.com/linux/static/stable/x86_64/docker-19.03.9.tgz"`
	list   string `name:"list path auto order install" help:"容器服务"`
}

func (s server) List(m *ice.Message, arg ...string) { s.Code.Source(m, "", arg...) }

func init() { ice.CodeModCmd(server{}) }
