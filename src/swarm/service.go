package swarm

import (
	"strings"

	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/mdb"
	kit "shylinux.com/x/toolkits"
)

const (
	SERVICE    = "service"
	SERVICE_ID = "SERVICE_ID"
	UPDATE     = "update"
	REPLICAS   = "replicas"
	IMAGE      = "image"
)

type service struct {
	client.Docker
	create  string `name:"create image* name replicas*=3"`
	scale   string `name:"scale replicas=3" help:"扩容"`
	upgrade string `name:"upgrade image*"`
	list    string `name:"list SERVICE_ID auto" help:"服务"`
}

func (s service) Create(m *ice.Message, arg ...string) {
	s.cmd(m.PushStream(), "", kit.Simple(s.Args(m, mdb.NAME, REPLICAS), m.Option(IMAGE))...)
}
func (s service) Remove(m *ice.Message, arg ...string) {
	s.cmd(m, "", kit.Select("", arg, 0))
}
func (s service) Scale(m *ice.Message, arg ...string) {
	s.cmd(m.PushStream(), UPDATE, kit.Simple(s.Args(m), m.Option(SERVICE_ID))...)
}
func (s service) Upgrade(m *ice.Message, arg ...string) {
	s.cmd(m.PushStream(), UPDATE, kit.Simple(s.Args(m), m.Option(SERVICE_ID))...)
}
func (s service) Restart(m *ice.Message, arg ...string) {
	s.cmd(m.PushStream(), UPDATE, "--force", m.Option(SERVICE_ID))
}
func (s service) List(m *ice.Message, arg ...string) {
	if len(arg) == 0 {
		m.SplitIndex(strings.ReplaceAll(kit.Replace(s.cmd(m, LS), "ID        ", "SERVICE_ID"), "_ ", "  "))
		m.Action(s.Create).PushAction(s.Scale, s.Upgrade, s.Restart, s.Inspect, s.Remove)
	} else {
		m.SplitIndex(kit.Replace(s.cmd(m, PS, arg[0]), "ID     ", "TASK_ID", "DESIRED STATE", "", "CURRENT STATE", ""))
		m.Action(s.Inspect, s.Logs)
	}
}
func (s service) Inspect(m *ice.Message, arg ...string) {
	m.Echo(s.cmd(m, "", kit.Select("", arg, 0))).ProcessInner()
}
func (s service) Logs(m *ice.Message, arg ...string) {
	m.Echo(s.cmd(m, "", kit.Select("", arg, 0))).ProcessInner()
}

func init() { ice.CodeModCmd(service{}) }

func (s service) cmd(m *ice.Message, cmd string, arg ...string) string {
	kit.If(len(arg) > 0 && arg[0] == "", func() { arg[0] = m.Option(SERVICE_ID) })
	kit.If(cmd == "", func() { cmd = m.ActionKey() })
	return s.Cmdx(m, kit.Simple(SERVICE, cmd, arg)...)
}
