package swarm

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
)

const (
	LS = "ls"
	PS = "ps"
	RM = "rm"
)

type Swarm struct {
	client.Docker
	list string `name:"list list" help:"集群管理"`
}

func (s Swarm) List(m *ice.Message, arg ...string) {
	ctx.DisplayStudio(m.Message, SERVICE, STACK, NODE, CONFIG, SECRET)
	s.Status(m)
}

func init() { ice.CodeModCmd(Swarm{}) }
