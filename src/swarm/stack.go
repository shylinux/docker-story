package swarm

import (
	"path"

	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	kit "shylinux.com/x/toolkits"
)

const (
	STACK  = "stack"
	DEPLOY = "deploy"
)

type stack struct {
	client.Docker
	create string `name:"create path* name*=demo"`
	list   string `name:"list STACK SERVICE auto" help:"堆栈"`
}

func (s stack) Inputs(m *ice.Message, arg ...string) {
	switch arg[0] {
	case nfs.PATH:
		m.Cmdy(nfs.DIR, m.Option(nfs.PATH), nfs.PATH, kit.Dict(nfs.DIR_REG, kit.ExtReg(nfs.YML)))
		m.Cmdy(nfs.DIR, path.Join(ice.SRC_TEMPLATE, m.PrefixKey())+nfs.PS, nfs.PATH)
	case mdb.NAME:
		m.Push(arg[0], path.Base(m.Option(nfs.PATH)))
	}
}
func (s stack) Create(m *ice.Message, arg ...string) {
	s.Cmds(m.PushStream(), DEPLOY, "-c", m.Option(nfs.PATH), m.Option(mdb.NAME))
}
func (s stack) Remove(m *ice.Message, arg ...string) {
	s.Cmds(m.PushStream(), RM, m.Option("STACK"))
}
func (s stack) List(m *ice.Message, arg ...string) {
	if len(arg) == 0 {
		m.SplitIndex(s.Cmds(m.Spawn(), LS)).RenameAppend("NAME", "STACK")
		m.PushAction(s.Remove).Action(s.Create)
	} else if len(arg) == 1 {
		m.SplitIndex(s.Cmds(m.Spawn(), "services", arg[0])).RenameAppend("NAME", "SERVICE")
	} else {
		m.SplitIndexReplace(s.Cmdx(m.Spawn(), SERVICE, PS, arg[1]), "DESIRED STATE", "DESIRED_STATE", "CURRENT STATE", "CURRENT_STATE")
	}
}

func init() { ice.CodeModCmd(stack{}) }
