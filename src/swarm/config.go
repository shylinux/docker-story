package swarm

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
)

const (
	CONFIG = "config"
)

type config struct {
	client.Docker
	create string `name:"create file* name*"`
	list   string `name:"list NAME auto" help:"配置"`
}

func (s config) Create(m *ice.Message, arg ...string) {
	s.Cmds(m, "", m.Option(mdb.NAME), m.Option(nfs.FILE))
}
func (s config) Prune(m *ice.Message, arg ...string) {}

func init() { ice.CodeModCmd(config{}) }
