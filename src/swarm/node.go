package swarm

import (
	"strings"

	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/tcp"
	"shylinux.com/x/icebergs/base/web"
	kit "shylinux.com/x/toolkits"
)

const (
	NODE    = "node"
	NODE_ID = "NODE_ID"
)

type node struct {
	client.Docker
	start   string `name:"start advertise-addr* data-path-port=5789 listen-addr=':2377'"`
	token   string `name:"token" help:"令牌"`
	leave   string `name:"leave" help:"退群"`
	demote  string `name:"demote" help:"降级"`
	promote string `name:"promote" help:"晋级"`
	active  string `name:"active" help:"活跃"`
	pause   string `name:"pause" help:"暂停"`
	drain   string `name:"drain" help:"清空"`
	list    string `name:"list NODE_ID auto" help:"节点"`
}

func (s node) Inputs(m *ice.Message, arg ...string) {
	switch arg[0] {
	case "advertise-addr":
		m.Cmdy(tcp.HOST).CutTo(aaa.IP, arg[0])
		m.Push(arg[0], kit.Value(web.PublicIP(m.Message), "query"))
	}
}
func (s node) Start(m *ice.Message, arg ...string) {
	s.Cmdx(m, kit.Simple("swarm", "init", s.Args(m))...)
}
func (s node) Leave(m *ice.Message, arg ...string) {
	m.Echo(s.Cmdx(m, "swarm", "leave", "-f"))
}
func (s node) Token(m *ice.Message, arg ...string) {
	m.Echo(s.Cmdx(m, "swarm", "join-token", "worker")).ProcessInner()
}
func (s node) Remove(m *ice.Message, arg ...string)  { s.cmd(m, "", "") }
func (s node) Inspect(m *ice.Message, arg ...string) { m.Echo(s.cmd(m, "", "")) }
func (s node) Promote(m *ice.Message, arg ...string) { s.cmd(m, "", "") }
func (s node) Demote(m *ice.Message, arg ...string)  { s.cmd(m, "", "") }
func (s node) Active(m *ice.Message, arg ...string)  { s.avail(m, "") }
func (s node) Pause(m *ice.Message, arg ...string)   { s.avail(m, "") }
func (s node) Drain(m *ice.Message, arg ...string)   { s.avail(m, "") }
func (s node) List(m *ice.Message, arg ...string) {
	if len(arg) == 0 {
		m.SplitIndex(kit.Replace(s.cmd(m, LS), "ID     ", NODE_ID, "MANAGER STATUS", "", "ENGINE VERSION", ""))
		m.Table(func(value ice.Maps) {
			button := []ice.Any{}
			switch value["AVAILABILITY"] {
			case "Active":
				button = append(button, s.Pause)
			case "Pause":
				button = append(button, s.Active, s.Drain)
			case "Drain":
				button = append(button, s.Active)
			}
			button = append(button, s.Inspect)
			switch value["MANAGER_STATUS"] {
			case "Leader", "Reachable":
				button = append(button, s.Demote)
			default:
				button = append(button, s.Promote)
			}
			switch value["STATUS"] {
			case "Down":
				button = append(button, s.Remove)
			}
			m.PushButton(button...)
		})
		if m.Length() == 0 {
			m.Action(s.Start)
		} else {
			m.Action(s.Token, s.Leave)
		}
	} else {
		arg[0] = strings.TrimSuffix(arg[0], " *")
		m.SplitIndex(kit.Replace(s.cmd(m, PS, arg[0]), "ID     ", "TASK_ID", "DESIRED STATE", "", "CURRENT STATE", ""))
	}
}

func init() { ice.CodeModCmd(node{}) }

func (s node) cmd(m *ice.Message, cmd string, arg ...string) string {
	kit.If(len(arg) > 0 && arg[0] == "", func() { arg[0] = strings.TrimSuffix(m.Option(NODE_ID), " *") })
	kit.If(cmd == "", func() { cmd = m.ActionKey() })
	return s.Cmdx(m, kit.Simple(NODE, cmd, arg)...)
}
func (s node) avail(m *ice.Message, avail string, arg ...string) {
	kit.If(avail == "", func() { avail = m.ActionKey() })
	s.cmd(m, UPDATE, "--availability="+avail, m.Option(NODE_ID))
}
