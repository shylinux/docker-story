package client

import (
	"os"
	"path"
	"strings"

	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/cli"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	"shylinux.com/x/icebergs/base/tcp"
	"shylinux.com/x/icebergs/base/web"
	kit "shylinux.com/x/toolkits"
)

const (
	IMAGE    = "image"
	IMAGE_ID = "IMAGE_ID"

	PUSH = "push"
	PULL = "pull"
	TAGS = "tags"

	REPOSITORY = "REPOSITORY"
	TAG        = "TAG"
)

type Image struct {
	Docker
	prefix string `data:"IMAGE ID"`
	create string `name:"create name*=contexts dir*=usr/publish/" help:"构建"`
	start  string `name:"start port dev cmd" help:"启动"`
	list   string `name:"list IMAGE_ID auto" help:"镜像"`
	tag    string `name:"tag image* tags*" help:"发版"`
	pull   string `name:"pull image*" help:"下载"`
	stats  string `name:"stats" help:"资源"`
	df     string `name:"df" help:"磁盘"`
}

func (s Image) Inputs(m *ice.Message, arg ...string) {
	if m.Option(ctx.ACTION) == mdb.CREATE && arg[0] == mdb.NAME {
		m.Cmdy(nfs.DIR, path.Join(ice.SRC_TEMPLATE, m.PrefixKey())+nfs.PS, mdb.NAME)
	} else {
		s.Docker.Inputs(m, arg...)
	}
}
func (s Image) Create(m *ice.Message, arg ...string) {
	m.Cmd(nfs.DEFS, path.Join(m.Option(nfs.DIR), "Dockerfile"), nfs.Template(m, m.Option(mdb.NAME)))
	s.Cmds(m.PushStream(), cli.BUILD, "-t", m.Option(mdb.NAME), m.Option(nfs.DIR))
}
func (s Image) Remove(m *ice.Message, arg ...string) {
	s.Cmds(m, RM, m.Option(REPOSITORY)+":"+m.Option(TAG))
}
func (s Image) Push(m *ice.Message, arg ...string) {
	s.Cmds(m.PushStream(), "", m.Option(REPOSITORY)+":"+m.Option(TAG))
}
func (s Image) Pull(m *ice.Message, arg ...string) {
	s.Cmds(m.PushStream(), "", m.Option(IMAGE))
}
func (s Image) Tag(m *ice.Message, arg ...string) {
	s.Cmds(m, "", m.Option(IMAGE_ID), m.Option(IMAGE)+":"+m.Option(TAG))
}
func (s Image) Start(m *ice.Message, arg ...string) {
	image := m.Option(IMAGE_ID)
	if m.Option(REPOSITORY) != "" && m.Option(TAG) != "" {
		image = m.Option(REPOSITORY) + ice.DF + m.Option(TAG)
	}
	opts, args := []string{}, []string{}
	if m.Option(tcp.PORT) != "" {
		opts = append(opts, "-p", m.Option(tcp.PORT))
	} else {
		opts = append(opts, "-P")
	}
	if m.Option(ice.DEV) == "" {
		opts = append(opts, "-e", "ctx_dev="+web.UserHost(m.Message))
	} else {
		opts = append(opts, "-e", "ctx_dev="+m.Option(ice.DEV))
	}
	kit.For([]string{"TZ", "LANG"}, func(key string) {
		opts = append(opts, "-e", key+"="+os.Getenv(key))
	})
	kit.If(m.Option(ice.CMD), func(cmd string) {
		args = append(args, cmd)
	})
	s.Cmdx(m, kit.Simple(cli.RUN, opts, "-dt", image, args)...)
	web.ToastSuccess(m.Message)
}

func (s Image) List(m *ice.Message, arg ...string) {
	if s.Docker.List(m, arg...); len(arg) == 0 {
		m.Table(func(value ice.Maps) {
			if strings.Contains(value[REPOSITORY], nfs.PS) {
				m.PushButton(s.Start, s.Tag, s.Push, s.Remove)
			} else {
				m.PushButton(s.Start, s.Tag, s.Remove)
			}
		}).Action(s.Create, s.Pull, s.Prune)
	}
}

func init() { ice.CodeModCmd(Image{}) }
