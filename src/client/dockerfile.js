Volcanos(chat.ONSYNTAX, {
	dockerfile: {prefix: {"#": code.COMMENT}, keyword: {
		"ARG": code.KEYWORD,
		"FROM": code.KEYWORD, "AS": code.KEYWORD,
		"WORKDIR": code.KEYWORD,
		"SHELL": code.KEYWORD,
		"COPY": code.KEYWORD,
		"ADD": code.KEYWORD,
		"ENV": code.KEYWORD,
		"RUN": code.KEYWORD,
		"CMD": code.KEYWORD,
		"EXPOSE": code.KEYWORD,
		"ENTRYPOINT": code.KEYWORD,
		"scratch": code.CONSTANT, "busybox": code.CONSTANT,
	}, include: ["sh"], func: function(can, push, text) {
		if (text.indexOf("FROM") == 0) { push(text) }
	}},
})

