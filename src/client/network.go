package client

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/mdb"
	kit "shylinux.com/x/toolkits"
)

const (
	NETWORK = "network"
)

type Network struct {
	Docker
	prefix string `data:"NETWORK ID"`
	create string `name:"create name* driver"`
	list   string `name:"list NAME auto" help:"网络"`
}

func (s Network) Create(m *ice.Message, arg ...string) {
	s.Cmds(m, "", kit.Simple(s.Args(m, DRIVER), m.Option(mdb.NAME))...)
}

func init() { ice.CodeModCmd(Network{}) }
