package client

import (
	"strings"

	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/mem"

	"shylinux.com/x/ice"
	icebergs "shylinux.com/x/icebergs"
	"shylinux.com/x/icebergs/base/cli"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	"shylinux.com/x/icebergs/base/tcp"
	"shylinux.com/x/icebergs/base/web"
	"shylinux.com/x/icebergs/core/code"
	kit "shylinux.com/x/toolkits"
)

const (
	DOCKER  = "docker"
	LS      = "ls"
	PS      = "ps"
	RM      = "rm"
	INSPECT = "inspect"
	CONFIGS = "configs"
	DRIVER  = "driver"
)

type Docker struct{ ice.Code }

func (s Docker) Inputs(m *ice.Message, arg ...string) {
	switch arg[0] {
	case IMAGE:
		m.Spawn().SplitIndexReplace(s.Cmdx(m, IMAGE, LS), "IMAGE ID", "").Table(func(value ice.Maps) {
			if value[TAG] == "latest" {
				m.Push(arg[0], value[REPOSITORY])
			} else {
				m.Push(arg[0], kit.Format("%s:%s", value[REPOSITORY], value[TAG]))
			}
		})
		m.Push(arg[0], cli.BUSYBOX, cli.ALPINE, cli.CENTOS, cli.UBUNTU)
	case ice.DEV:
		m.Push(arg[0], web.UserHost(m.Message)).Cmd(web.SPIDE, func(value ice.Maps) { m.Push(arg[0], value[web.CLIENT_URL]) })
	case ice.CMD:
		m.Push(arg[0], code.BASH, code.SH)
	case tcp.PORT:
		m.Cmdy(CONTAINER, CONFIGS).Cut(PORT_SOURCE, mdb.NAME).RenameAppend(PORT_SOURCE, arg[0])
		m.Cmdy(tcp.PORT, mdb.INPUTS, arg)
	}
}
func (s Docker) Remove(m *ice.Message, arg ...string) {
	s.Cmds(m, "", m.Option(kit.Select("NAME", strings.ReplaceAll(m.Config("prefix"), " ", "_"))))
}
func (s Docker) Prune(m *ice.Message, arg ...string) { s.Cmds(m, "", "-f") }
func (s Docker) List(m *ice.Message, arg ...string) {
	if len(arg) == 0 {
		m.SplitIndexReplace(s.Cmds(m, LS), kit.Select(strings.ToUpper(m.CommandKey())+" ID", m.Config("prefix")), "")
		m.PushAction(s.Remove).Action(mdb.CREATE, s.Prune)
	} else {
		m.Echo(s.Cmds(m, INSPECT, arg[0]))
	}
}
func (s Docker) Status(m *ice.Message, arg ...string) {
	m.StatusTimeCount("SIZE", strings.Join(kit.Slice(s.Df(m.Spawn()).Appendv("SIZE"), 0, 3), nfs.PS),
		"disk", kit.Renders("{{.Used|FmtSize}}/{{.Total|FmtSize}}", kit.Ignore(disk.Usage(kit.Path("")))),
		"mem", kit.Renders("{{.Used|FmtSize}}/{{.Total|FmtSize}}", kit.Ignore(mem.VirtualMemory())),
		"cpu", kit.Format("%d/%d", kit.Ignore(cpu.Counts(true)), kit.Ignore(cpu.Counts(false))),
	)
}
func (s Docker) Stats(m *ice.Message, arg ...string) {
	m.SplitIndexReplace(s.Cmdx(m, "stats", "-a", "--no-stream"), "CONTAINER ID", "", "CPU %", "", "MEM USAGE / LIMIT", "", "MEM %", "", "NET I/O", "", "BLOCK I/O", "")
	s.Status(m)
}
func (s Docker) Df(m *ice.Message, arg ...string) *ice.Message {
	m.SplitIndex(s.Cmdx(m, cli.SYSTEM, "df"))
	return m
}
func (s Docker) Args(m *ice.Message, arg ...string) (args []string) {
	if len(arg) == 0 {
		m.Search(m.CommandKey(), func(key string, cmd *icebergs.Command) {
			kit.For(cmd.Actions[m.ActionKey()].List, func(value ice.Map) { arg = append(arg, kit.Format(value[mdb.NAME])) })
		})
	}
	kit.For(arg, func(key string) { kit.If(m.Option(key), func(value string) { args = append(args, "--"+key, value) }) })
	return args
}
func (s Docker) Cmds(m *ice.Message, cmd string, arg ...string) string {
	return s.Cmdx(m, kit.Simple(m.CommandKey(), kit.Select(m.ActionKey(), cmd), arg)...)
}
func (s Docker) Cmdx(m *ice.Message, arg ...string) string {
	if msg := m.Cmd(cli.SYSTEM, DOCKER, arg); m.Warn(!cli.IsSuccess(msg.Message), msg.Result()) {
		web.Toast(m.Message, msg.Result(), "", "10000")
		return ""
	} else {
		return msg.Result()
	}
}
func (s Docker) short(m *ice.Message, arg ...string) string {
	return kit.Cut(kit.Select(m.Option(CONTAINER_ID), arg, 0), 12)
}
