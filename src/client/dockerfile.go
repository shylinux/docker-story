package client

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	"shylinux.com/x/icebergs/base/web/html"
	"shylinux.com/x/icebergs/core/code"
	kit "shylinux.com/x/toolkits"
)

type dockerfile struct{ ice.Lang }

func (s dockerfile) Init(m *ice.Message, arg ...string) {
	s.Lang.Init(m, nfs.SCRIPT, m.Resource(""))
}
func (s dockerfile) Render(m *ice.Message, arg ...string) {
	s.xterm(m, arg...)
}
func (s dockerfile) Engine(m *ice.Message, arg ...string) {
	ctx.Process(m.Message, ice.GetTypeKey(Client{}), nil, arg...)
}

func init() { ice.CodeModCmd(dockerfile{}) }

func (s dockerfile) xterm(m *ice.Message, arg ...string) {
	ctx.Process(m.Message, code.XTERM, func() []string {
		m.Push(ctx.STYLE, html.OUTPUT)
		if ls := kit.Simple(kit.UnMarshal(m.Option(ctx.ARGS))); len(ls) > 0 {
			return ls
		}
		return []string{mdb.TYPE, code.SH, nfs.PATH, kit.Select("", kit.Dir(arg[2], arg[1]), arg[2] != ice.SRC)}
	}, arg...)
}
