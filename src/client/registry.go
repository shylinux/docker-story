package client

import (
	"path"
	"strings"

	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	"shylinux.com/x/icebergs/base/web"
	kit "shylinux.com/x/toolkits"
)

type Registry struct {
	Docker
	addr string `data:"https://registry.shylinux.com"`
	pull string `name:"pull" help:"下载"`
	list string `name:"list image tags auto" help:"仓库"`
}

func (s Registry) Pull(m *ice.Message, arg ...string) {
	s.Cmdx(m.PushStream(), PULL, m.Option(IMAGE)+":"+m.Option(TAGS))
}
func (s Registry) List(m *ice.Message, arg ...string) {
	addr := path.Base(s._addr(m))
	if len(arg) == 0 || arg[0] == "" {
		list := s.get(m, "/v2/_catalog")
		kit.For(kit.Value(list, "repositories"), func(image string) {
			m.Push(IMAGE, path.Join(addr, image))
		})
	} else if arg[0] = strings.TrimPrefix(arg[0], addr+nfs.PS); len(arg) == 1 || arg[1] == "" {
		list := s.get(m, kit.Format("/v2/%s/tags/list", arg[0]))
		kit.For(kit.Value(list, TAGS), func(tags ice.Any) {
			m.Push(IMAGE, addr+nfs.PS+arg[0]).Push(TAGS, tags)
		})
		m.PushAction(s.Pull)
	} else if len(arg) == 2 || arg[2] == "" {
		list := s.get(m, kit.Format("/v2/%s/manifests/%s", arg[0], arg[1]))
		m.Echo(kit.Formats(list))
		kit.For(kit.Value(list, "history"), func(value ice.Map) {
			value = kit.UnMarshal(kit.Value(value, "v1Compatibility")).(kit.Map)
			m.Push(mdb.TIME, kit.Value(value, "created")).Push(mdb.ID, kit.Value(value, mdb.ID))
			m.Push("config", kit.Select(kit.Formats(kit.Value(value, "config")), kit.Formats(kit.Value(value, "container_config"))))
			m.Push("comment", kit.Format(kit.Value(value, "comment")))
		})
	}
	m.StatusTimeCount("addr", s._addr(m))
}

func init() { ice.Cmd("web.code.docker.registryclient", Registry{}) }

func (s Registry) get(m *ice.Message, p string) ice.Any {
	return web.SpideGet(m.Message, s._addr(m)+p)
}
func (s Registry) _addr(m *ice.Message) string {
	return mdb.Config(m, "addr")
}
