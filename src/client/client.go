package client

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/core/chat/macos"
	"shylinux.com/x/icebergs/core/code"
)

type Client struct {
	Docker
	list string `name:"list list" help:"容器管理"`
}

func (s Client) Init(m *ice.Message, arg ...string) {
	m.Cmd(macos.APPLICATIONS, code.INSTALL, DOCKER, m.PrefixKey(), mdb.ICON, m.Resource("docker.png"))
}
func (s Client) List(m *ice.Message, arg ...string) {
	ctx.DisplayStudio(m.Message, CONTAINER, IMAGE, "registryclient", NETWORK, VOLUME)
	s.Status(m)
}

func init() { ice.CodeModCmd(Client{}) }
