package client

import (
	"os"
	"path"
	"strings"

	"shylinux.com/x/ice"
	icebergs "shylinux.com/x/icebergs"
	"shylinux.com/x/icebergs/base/cli"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	"shylinux.com/x/icebergs/base/tcp"
	"shylinux.com/x/icebergs/base/web"
	kit "shylinux.com/x/toolkits"
)

const (
	CONTAINER    = "container"
	CONTAINER_ID = "CONTAINER_ID"

	PORT = "port"
	CONF = "conf"
	DATA = "data"
	NAME = "name"

	PORT_SOURCE = "port_source"
	PORT_TARGET = "port_target"
	CONF_SOURCE = "conf_source"
	CONF_TARGET = "conf_target"
	DATA_SOURCE = "data_source"
	DATA_TARGET = "data_target"
	LIST_PREFIX = "list_prefix"
	HOST_DOMAIN = "host_domain"
)

type Container struct {
	Docker
	create  string `name:"create port conf data name password=root restart=no,always" help:"创建"`
	list    string `name:"list CONTAINER_ID auto" help:"容器"`
	inspect string `name:"inspect" help:"详情"`
	stats   string `name:"stats" help:"资源"`
}

func (s Container) Inputs(m *ice.Message, arg ...string) {
	switch arg[0] {
	case PORT:
		m.Push(arg[0], m.Config(PORT_SOURCE))
	case CONF:
		m.Push(arg[0], m.Config(CONF_SOURCE))
	case DATA:
		m.Push(arg[0], m.Config(DATA_SOURCE))
	case NAME:
		p := m.Config(LIST_PREFIX)
		if m.Push(arg[0], p); strings.Contains(p, nfs.PS) {
			m.Push(arg[0], path.Base(m.Config(LIST_PREFIX)))
		}
	}
}
func (s Container) Create(m *ice.Message, arg ...string) {
	opts := []string{}
	kit.If(m.Option(PORT), func(p string) {
		opts = append(opts, "-p", p+":"+m.Config(PORT_TARGET))
	}, func() {
		opts = append(opts, "-P")
	})
	kit.If(m.Option(CONF), func(p string) {
		opts = append(opts, "-v", s.local(m, p)+":"+m.Config(CONF_TARGET))
	})
	kit.If(m.Option(DATA), func(p string) {
		opts = append(opts, "-v", s.local(m, p)+":"+m.Config(DATA_TARGET))
	})
	kit.For([]string{"TZ", "LANG"}, func(key string) {
		opts = append(opts, "-e", key+"="+os.Getenv(key))
	})
	opts = append(opts, "--privileged")
	s.Cmdx(m.PushStream(), kit.Simple(cli.RUN, opts, s.Args(m, "name", "restart"), m.Optionv(ctx.OPTS), "-dt", m.Config(LIST_PREFIX), m.Optionv(ctx.ARGS))...)
}
func (s Container) Remove(m *ice.Message, arg ...string) {
	s.Cmds(m, RM, s.id(m))
}
func (s Container) Modify(m *ice.Message, arg ...string) {
	switch arg[0] {
	case "NAMES":
		s.Cmds(m, mdb.RENAME, m.Option(CONTAINER_ID), arg[1])
	}
}
func (s Container) Stop(m *ice.Message, arg ...string)    { s.Cmds(m, "", s.id(m)) }
func (s Container) Start(m *ice.Message, arg ...string)   { s.Cmds(m, "", s.id(m)) }
func (s Container) Restart(m *ice.Message, arg ...string) { s.Cmds(m, "", s.id(m)) }
func (s Container) List(m *ice.Message, arg ...string) *ice.Message {
	if len(arg) == 0 {
		prefix := m.Config(LIST_PREFIX)
		m.Spawn().SplitIndexReplace(s.Cmds(m, LS, "-a"), "CONTAINER ID", "").Table(func(index int, value ice.Maps, head []string) {
			if !strings.HasPrefix(value["IMAGE"], prefix) {
				return
			} else if m.Push("", value, head); strings.HasPrefix(value["STATUS"], "Up") {
				if value["PORTS"] == "" {
					m.PushButton(s.Xterm, s.Inspect, s.Logs, s.Stop)
				} else if strings.Contains(value["PORTS"], "9020") {
					m.PushButton(s.Portal, s.Xterm, s.Stop)
				} else {
					m.PushButton(s.Open, s.Xterm, s.Inspect, s.Logs, s.Stop)
				}
			} else {
				m.PushButton(s.Start, s.Inspect, s.Logs, s.Remove)
			}
		})
		m.Action(s.Stats, s.Configs, s.Prune)
	} else {
		m.Split(s.Cmdx(m, "top", arg[0])).PushAction(s.Stop).Action(s.Portal, s.Xterm, s.Open, s.Logs)
		m.StatusTimeCount(tcp.HOST, s.hostport(m, arg[0]))
	}
	return m
}
func (s Container) Inspect(m *ice.Message, arg ...string) {
	m.Echo(s.Cmds(m, "", s.id(m))).ProcessInner()
}
func (s Container) Logs(m *ice.Message, arg ...string) {
	m.Echo(s.Cmds(m, "", s.id(m))).ProcessInner()
}
func (s Container) Open(m *ice.Message, arg ...string) {
	if p := m.Config(HOST_DOMAIN); p != "" {
		m.ProcessOpen(p)
	} else {
		m.ProcessOpen(s.hostport(m, m.Option(CONTAINER_ID)))
	}
}
func (s Container) Portal(m *ice.Message, arg ...string) {
	s.Code.Iframe(m, "", s.dev(m, arg...), arg...)
}
func (s Container) Vimer(m *ice.Message, arg ...string) {
	s.Code.Iframe(m, "", m.Options(ice.MSG_USERWEB, s.dev(m, arg...), ice.MSG_USERPOD, "").MergePodCmd(s.short(m, arg...), web.CODE_VIMER), arg...)
}
func (s Container) Xterm(m *ice.Message, arg ...string) {
	ctx.ProcessField(m.Message, web.CODE_XTERM, func() []string {
		return []string{kit.Format("docker exec -it %s %s", s.id(m), "sh")}
	}, arg...)
}
func (s Container) Configs(m *ice.Message, arg ...string) {
	m.Travel(func(p *icebergs.Context, s *icebergs.Context, key string, cmd *icebergs.Command) {
		if s.Prefix() == "web.code.docker" && m.Config(PORT_SOURCE) != "" {
			m.Push(mdb.NAME, key).Push(mdb.HELP, cmd.Help)
			kit.For([]string{PORT_SOURCE, PORT_TARGET, CONF_TARGET, DATA_TARGET, LIST_PREFIX, HOST_DOMAIN}, func(key string) { m.Push(key, m.Config(key)) })
		}
	}).Sort(PORT_SOURCE, "int").StatusTimeCount()
}

func init() { ice.CodeModCmd(Container{}) }

func (s Container) Cmds(m *ice.Message, cmd string, arg ...string) string {
	return s.Cmdx(m, kit.Simple(CONTAINER, kit.Select(m.ActionKey(), cmd), arg)...)
}
func (s Container) id(m *ice.Message, arg ...string) string {
	return kit.Select(m.Option(CONTAINER_ID), arg, 0)
}
func (s Container) dev(m *ice.Message, arg ...string) string {
	return s.Cmdx(m, cli.EXEC, s.short(m, arg...), "./bin/ice.bin", "web.admin")
}
func (s Container) hostport(m *ice.Message, id string) (p string) {
	kit.For(kit.Value(kit.UnMarshal(s.Cmds(m, INSPECT, id)), "0.HostConfig.PortBindings"), func(key string, value ice.Any) {
		kit.For(value, func(value ice.Map) { p = web.HostPort(m.Message, "", kit.Format(value["HostPort"])) })
	})
	return
}
func (s Container) local(m *ice.Message, p string) string {
	kit.If(!nfs.Exists(m, p), func() {
		m.Debug("what %v", p)
		if strings.HasSuffix(p, nfs.PS) {
			m.Debug("what %v", p)
			nfs.MkdirAll(m, p)
			os.Chmod(p, 0777)
		} else {
			m.Debug("what %v", p)
			m.Cmd(nfs.DEFS, p, "")
			os.Chmod(p, 0666)
		}
	})
	return kit.Path(p)
}
