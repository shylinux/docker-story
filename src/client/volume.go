package client

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/mdb"
	kit "shylinux.com/x/toolkits"
)

const (
	VOLUME = "volume"
)

type Volume struct {
	Docker
	prefix string `data:"VOLUME NAME"`
	create string `name:"create name* driver"`
	list   string `name:"list VOLUME_NAME auto" help:"存储"`
}

func (s Volume) Create(m *ice.Message, arg ...string) {
	s.Cmds(m, "", kit.Simple(s.Args(m, DRIVER), m.Option(mdb.NAME))...)
}

func init() { ice.CodeModCmd(Volume{}) }
