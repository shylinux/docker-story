package yaml

import (
	"path"

	yml "gopkg.in/yaml.v3"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/mdb"
	"shylinux.com/x/icebergs/base/nfs"
	kit "shylinux.com/x/toolkits"
)

type yaml struct {
	ice.Lang
}

func (s yaml) Init(m *ice.Message, arg ...string) {
	s.Lang.Init(m, nfs.SCRIPT, m.Resource(""))
}
func (s yaml) Render(m *ice.Message, arg ...string) {
	if s.process(m, arg...) {
		return
	}
	m.Echo(kit.Formats(s.parse(m, arg...))).Display("/plugin/story/json.js")
}
func (s yaml) Engine(m *ice.Message, arg ...string) {
	if s.process(m, arg...) {
		return
	}
	defer m.StatusTimeCount()
	m.OptionFields(mdb.DETAIL)
	kit.For(kit.KeyValue(nil, "", s.parse(m, arg...)), func(k string, v string) { m.Push(k, v) })
}

func init() { ice.Cmd("web.code.yml", yaml{}) }
func init() { ice.Cmd("web.code.yaml", yaml{}) }

func (s yaml) parse(m *ice.Message, arg ...string) ice.Map {
	data := kit.Dict()
	yml.Unmarshal([]byte(m.Cmdx(nfs.CAT, path.Join(arg[2], arg[1]))), &data)
	return data
}
func (s yaml) process(m *ice.Message, arg ...string) bool {
	if kit.IsIn(kit.TrimExt(arg[1], m.CommandKey()), "docker-stack", "docker-compose") {
		ctx.Process(m.Message, "web.code.docker.swarm", nil, arg...)
		return true
	}
	return false
}
