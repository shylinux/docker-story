package {{.Option "zone"}}

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type {{.Option "name"}} struct {
	client.Container
	port_source string `data:"8080"`
	port_target string `data:"8080"`
	conf_source string `data:"./var/conf/{{.Option "name"}}/"`
	conf_target string `data:"/{{.Option "name"}}/config/"`
	data_source string `data:"./var/data/{{.Option "name"}}/"`
	data_target string `data:"/{{.Option "name"}}/data/"`
	list_prefix string `data:"{{.Option "name"}}"`

	list string `name:"list CONTAINER_ID auto create" help:"容器服务"`
}

func init() { ice.CodeModCmd({{.Option "name"}}{}) }
