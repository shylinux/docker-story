package drawio

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type drawio struct {
	client.Container
	port_source string `data:"8005"`
	port_target string `data:"8080"`
	conf_source string `data:"./var/conf/drawio/"`
	conf_target string `data:"/drawio/config/"`
	data_source string `data:"./var/data/drawio/"`
	data_target string `data:"/drawio/data/"`
	list_prefix string `data:"jgraph/drawio"`
	host_domain string `data:"https://draw.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"绘图"`
}

func init() { ice.CodeModCmd(drawio{}) }
