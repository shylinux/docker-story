package zookeeper

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type zookeeper struct {
	client.Container
	port_source string `data:"2181"`
	port_target string `data:"2181"`
	conf_source string `data:"./var/conf/zookeeper/zoo.cfg"`
	conf_target string `data:"/opt/zookeeper-3.4.13/conf/zoo.cfg"`
	data_source string `data:"./var/data/zookeeper/"`
	data_target string `data:"/opt/zookeeper-3.4.13/data/"`
	list_prefix string `data:"wurstmeister/zookeeper"`

	list string `name:"list CONTAINER_ID auto create" help:"服务管理"`
}

func init() { ice.CodeModCmd(zookeeper{}) }
