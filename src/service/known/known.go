package known

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type known struct {
	client.Container
	port_source string `data:"8009"`
	port_target string `data:"9000"`
	conf_source string `data:"./var/conf/known/"`
	conf_target string `data:"/known/config/"`
	data_source string `data:"./var/data/known/"`
	data_target string `data:"/known/data/"`
	list_prefix string `data:"known"`

	list string `name:"list CONTAINER_ID auto create" help:"容器服务"`
}

func init() { ice.CodeModCmd(known{}) }
