package kafka

import (
	"strings"

	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
	kit "shylinux.com/x/toolkits"
)

type kafka struct {
	client.Container
	port_source string `data:"9092"`
	port_target string `data:"9092"`
	conf_source string `data:"./var/conf/kafka/server.properties"`
	conf_target string `data:"/opt/kafka/config/server.properties"`
	data_source string `data:"./var/data/kafka/"`
	data_target string `data:"/kafka/"`
	list_prefix string `data:"wurstmeister/kafka"`

	create string `name:"create port conf data zookeeper*='zookeeper:2181'" help:"创建"`
	list   string `name:"list CONTAINER_ID auto create" help:"消息队列"`
}

func (s kafka) Inputs(m *ice.Message, arg ...string) {
	switch arg[0] {
	case "zookeeper":
		m.Cmd(s.Container).Table(func(value ice.Maps) {
			kit.For(kit.Split(value["PORTS"], ", "), func(ports string) {
				if strings.Contains(ports, "->") {
					m.Push(arg[0], value["NAMES"]+":"+kit.Select("", kit.Split(strings.Split(ports, "->")[0], ":"), -1))
					m.Push("IMAGE", value["IMAGE"])
				}
			})
		})
	default:
		s.Container.Inputs(m, arg...)
	}
}
func (s kafka) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.OPTS, []string{
		"-e", "KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://127.0.0.1:" + m.Option(client.PORT),
		"-e", "KAFKA_LISTENERS=PLAINTEXT://127.0.0.1:" + m.Option(client.PORT),
		"-e", "KAFKA_ZOOKEEPER_CONNECT=" + m.Option("zookeeper"),
		"--link", kit.Split(m.Option("zookeeper"), ":")[0],
	})
	s.Container.Create(m, arg...)
}
func init() { ice.CodeModCmd(kafka{}) }
