package postgres

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/ctx"

	"shylinux.com/x/docker-story/src/client"
)

type postgres struct {
	client.Container
	port_source string `data:"5040"`
	port_target string `data:"5040"`
	conf_source string `data:"./var/conf/postgres/"`
	conf_target string `data:"/etc/postgresql/"`
	data_source string `data:"./var/data/postgres/"`
	data_target string `data:"/var/lib/postgresql/data/"`
	list_prefix string `data:"postgres"`

	list string `name:"list CONTAINER_ID auto create" help:"容器服务"`
}

func (s postgres) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.OPTS, []string{"-e", "POSTGRES_PASSWORD=" + m.Option(aaa.PASSWORD)})
	s.Container.Create(m, arg...)
}

func init() { ice.CodeModCmd(postgres{}) }
