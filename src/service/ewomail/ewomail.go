package ewomail

import (
	"strings"

	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/web"
	kit "shylinux.com/x/toolkits"

	"shylinux.com/x/docker-story/src/client"
)

type ewomail struct {
	client.Container
	smtp_port_source     string `data:"25"`
	smtp_port_target     string `data:"25"`
	imap_port_source     string `data:"143"`
	imap_port_target     string `data:"143"`
	pop3_port_source     string `data:"110"` // 109
	pop3_port_target     string `data:"110"` // 109
	smtp_ssh_port_source string `data:"465"` // 587
	smtp_ssh_port_target string `data:"465"` // 587
	imap_ssh_port_source string `data:"993"`
	imap_ssh_port_target string `data:"993"`
	pop3_ssh_port_source string `data:"995"`
	pop3_ssh_port_target string `data:"995"`
	admin_port_source    string `data:"8001"`
	admin_port_target    string `data:"8080"`

	port_source  string `data:"8000"`
	port_target  string `data:"80"`
	conf_source  string `data:"./var/conf/ewomail/"`
	conf_target  string `data:"/ewomail/mail/"`
	data_source  string `data:"./var/data/ewomail/"`
	data_target  string `data:"/ewomail/mysql/data/"`
	list_prefix  string `data:"bestwu/ewomail"`
	host_domain  string `data:"https://mail.shylinux.com"`
	admin_domain string `data:"https://mails.shylinux.com"`

	create string `name:"create domain* ssh=no,yes smtp_port* imap_port* admin_port* port conf data name" help:"创建"`
	list   string `name:"list CONTAINER_ID auto create" help:"邮箱"`
}

func (s ewomail) Inputs(m *ice.Message, arg ...string) {
	switch arg[0] {
	case web.DOMAIN:
		m.Push(arg[0], m.Config("host_domain"))
	case "admin_port":
		m.Push(arg[0], m.Config("admin_port_source"))
	case "smtp_port", "imap_port":
		if m.Option("ssh") == "yes" {
			m.Push(arg[0], m.Config(strings.Replace(arg[0], "_port", "_ssh_port", 1)+"_source"))
		} else {
			m.Push(arg[0], m.Config(arg[0]+"_source"))
		}
	default:
		s.Container.Inputs(m, arg...)
	}
}
func (s ewomail) Create(m *ice.Message, arg ...string) {
	host := kit.TrimPrefix(m.Option(web.DOMAIN), "https://", "http://")
	opts := []string{"-h", host, "-p", m.Option("admin_port") + ":" + m.Config("admin_port_target")}
	opts = append(opts, "-e", "WEBMAIL_URL="+m.Option(web.DOMAIN))
	opts = append(opts, "-e", "TITLE="+host)
	kit.For([]string{"smtp_port", "imap_port"}, func(k string) {
		kit.If(m.Option(k), func(p string) {
			kit.If(m.Option("ssh") == "yes", func() { k = strings.Replace(k, "_port", "_ssh_port", 1) })
			opts = append(opts, "-p", p+":"+m.Config(k+"_target"))
		})
	})
	m.Options(ctx.OPTS, opts)
	s.Container.Create(m, arg...)
}

func init() { ice.CodeModCmd(ewomail{}) }
