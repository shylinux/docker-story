package coder

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/ctx"

	"shylinux.com/x/docker-story/src/client"
)

type coder struct {
	client.Container
	port_source string `data:"8080"`
	port_target string `data:"8080"`
	conf_source string `data:"./var/conf/coder/config.yml"`
	conf_target string `data:"/home/coder/.config/code-server/config.yaml"`
	data_source string `data:"./var/data/coder/"`
	data_target string `data:"/home/coder/.local/share/code-server/"`
	list_prefix string `data:"codercom/code-server"`
	host_domain string `data:"https://code.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"编辑器"`
}

func (s coder) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.OPTS, []string{"-e", "PASSWORD=" + m.Option(aaa.PASSWORD)})
	s.Container.Create(m, arg...)
}

func init() { ice.CodeModCmd(coder{}) }
