package rocketchat

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type rocketchat struct {
	client.Container
	port_source string `data:"8008"`
	port_target string `data:"3000"`
	conf_source string `data:"./var/conf/rocketchat/"`
	conf_target string `data:"/rocketchat/config/"`
	data_source string `data:"./var/data/rocketchat/"`
	data_target string `data:"/rocketchat/data/"`
	list_prefix string `data:"registry.rocket.chat/rocketchat/rocket.chat"`
	host_domain string `data:"https://chat.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"聊天软件"`
}

func init() { ice.CodeModCmd(rocketchat{}) }
