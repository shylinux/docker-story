package consul

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type consul struct {
	client.Container
	port_source string `data:"8500"`
	port_target string `data:"8500"`
	conf_source string `data:"./var/conf/consul/"`
	conf_target string `data:"/consul/config/"`
	data_source string `data:"./var/data/consul/"`
	data_target string `data:"/consul/data/"`
	list_prefix string `data:"hashicorp/consul"`

	list string `name:"list CONTAINER_ID auto create" help:"服务发现"`
}

func init() { ice.CodeModCmd(consul{}) }
