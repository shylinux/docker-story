package minio

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/ctx"
)

type minio struct {
	client.Container
	port_source string `data:"9009"`
	port_target string `data:"9009"`
	conf_source string `data:"./var/conf/minio/"`
	conf_target string `data:"/root/.minio/"`
	data_source string `data:"./var/data/minio/"`
	data_target string `data:"/data/"`
	list_prefix string `data:"minio/minio"`
	host_domain string `data:"https://bucket.shylinux.com"`
	create      string `name:"create port conf data name username*=root password*=root1234" help:"创建"`

	list string `name:"list CONTAINER_ID auto create" help:"文件存储"`
}

func (s minio) Create(m *ice.Message, arg ...string) {
	m.Options(ctx.OPTS, []string{
		"-e", "MINIO_ROOT_USER=" + m.Option(aaa.USERNAME),
		"-e", "MINIO_ROOT_PASSWORD=" + m.Option(aaa.PASSWORD),
		"-p", "9000:9000",
	})
	m.Options(ctx.ARGS, []string{"server", "/data", "--console-address", ":9009", "-address", ":9000"})
	s.Container.Create(m, arg...)
}
func init() { ice.CodeModCmd(minio{}) }
