package mysql

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/ctx"
)

type mysql struct {
	client.Container
	port_source string `data:"3306"`
	port_target string `data:"3306"`
	conf_source string `data:"./var/conf/mysql/"`
	conf_target string `data:"/etc/mysql/conf.d/"`
	data_source string `data:"./var/data/mysql/"`
	data_target string `data:"/var/lib/mysql/"`
	list_prefix string `data:"mysql"`

	list string `name:"list CONTAINER_ID auto create" help:"数据库"`
}

func (s mysql) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.OPTS, []string{"-e", "MYSQL_ROOT_PASSWORD=" + m.Option(aaa.PASSWORD)})
	s.Container.Create(m, arg...)
}
func init() { ice.CodeModCmd(mysql{}) }
