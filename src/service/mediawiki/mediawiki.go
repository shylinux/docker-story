package mediawiki

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type mediawiki struct {
	client.Container
	port_source string `data:"8006"`
	port_target string `data:"80"`
	conf_source string `data:"./var/conf/mediawiki/"`
	conf_target string `data:"/var/www/html/"`
	data_source string `data:"./var/data/mediawiki/"`
	data_target string `data:"/var/www/data/"`
	list_prefix string `data:"mediawiki"`
	host_domain string `data:"https://wiki.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"百科"`
}

func init() { ice.CodeModCmd(mediawiki{}) }
