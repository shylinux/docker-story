package elasticsearch

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type elasticsearch struct {
	client.Container
	port_source string `data:"9200"`
	port_target string `data:"9200"`
	conf_source string `data:"./var/conf/elasticsearch/elasticsearch.yml"`
	conf_target string `data:"/etc/elasticsearch/elasticsearch.yml"`
	data_source string `data:"./var/data/elasticsearch/"`
	data_target string `data:"/usr/share/elasticsearch/data/"`
	list_prefix string `data:"elasticsearch"`

	list string `name:"list CONTAINER_ID auto create" help:"搜索引擎"`
}

func init() { ice.CodeModCmd(elasticsearch{}) }
