package pulsar

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type pulsar struct {
	client.Container
	port_source string `data:"6650"`
	port_target string `data:"6650"`
	conf_source string `data:"./var/conf/pulsar/"`
	conf_target string `data:"/pulsar/conf/"`
	data_source string `data:"./var/data/pulsar/"`
	data_target string `data:"/pulsar/data/"`
	list_prefix string `data:"apachepulsar/pulsar"`

	list string `name:"list CONTAINER_ID auto create" help:"容器服务"`
}

func init() { ice.CodeModCmd(pulsar{}) }
