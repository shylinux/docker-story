package redis

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/ctx"
)

type redis struct {
	client.Container
	port_source string `data:"6379"`
	port_target string `data:"6379"`
	conf_source string `data:"./var/conf/redis/"`
	conf_target string `data:"/etc/redis.conf.d/"`
	data_source string `data:"./var/data/redis/"`
	data_target string `data:"/data/"`
	list_prefix string `data:"redis"`

	list string `name:"list CONTAINER_ID auto create" help:"数据缓存"`
}

func (s redis) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.ARGS, []string{"redis-server", "--requirepass", m.Option(aaa.PASSWORD)})
	s.Container.Create(m, arg...)
}
func init() { ice.CodeModCmd(redis{}) }
