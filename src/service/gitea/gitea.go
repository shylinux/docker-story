package gitea

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type gitea struct {
	client.Container
	port_source string `data:"8081"`
	port_target string `data:"3000"`
	conf_source string `data:"./var/conf/gitea/app.ini"`
	conf_target string `data:"/data/gitea/conf/app.ini"`
	data_source string `data:"./var/data/gitea/"`
	data_target string `data:"/data/"`
	list_prefix string `data:"gitea/gitea"`
	host_domain string `data:"https://repos.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"代码库"`
}

func init() { ice.CodeModCmd(gitea{}) }
