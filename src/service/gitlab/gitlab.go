package gitlab

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/ctx"
	"shylinux.com/x/icebergs/base/tcp"
	"shylinux.com/x/icebergs/base/web"
)

type gitlab struct {
	client.Container
	port_source string `data:"8081"`
	port_target string `data:"80"`
	conf_source string `data:"./var/conf/gitlab/gitlab.rb"`
	conf_target string `data:"/etc/gitlab/gitlab.rb"`
	data_source string `data:"./var/data/gitlab/"`
	data_target string `data:"/var/opt/gitlab/"`
	list_prefix string `data:"gitlab/gitlab-ce"`
	host_domain string `data:"https://repos.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"代码库"`
}

func (s gitlab) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.OPTS, []string{
		"--hostname", tcp.PublishLocalhost(m.Message, web.UserWeb(m.Message).Hostname()),
		// "-e", "GITLAB_ROOT_PASSWORD=" + m.Option(aaa.PASSWORD),
	})
	s.Container.Create(m, arg...)
}
func init() { ice.CodeModCmd(gitlab{}) }
