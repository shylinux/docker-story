package teslamate

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type teslamate struct {
	client.Container
	port_source string `data:"8080"`
	port_target string `data:"80"`
	conf_source string `data:"./var/conf/coder/config.yml"`
	conf_target string `data:"/home/coder/.config/code-server/config.yaml"`
	data_source string `data:"./var/data/coder/"`
	data_target string `data:"/home/coder/.local/share/code-server/"`
	list_prefix string `data:"onlyoffice/documentserver"`

	list string `name:"list CONTAINER_ID auto create" help:"文档"`
}

func init() { ice.CodeModCmd(teslamate{}) }
