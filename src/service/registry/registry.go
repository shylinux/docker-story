package registry

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type registry struct {
	client.Container
	port_source string `data:"5000"`
	port_target string `data:"5000"`
	conf_source string `data:"./var/conf/registry/"`
	conf_target string `data:"/registry/config/"`
	data_source string `data:"./var/data/registry/"`
	data_target string `data:"/registry/data/"`
	list_prefix string `data:"registry"`
	host_domain string `data:"https://registry.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"容器服务"`
}

func init() { ice.CodeModCmd(registry{}) }
