package mongo

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type mongo struct {
	client.Container
	port_source string `data:"27017"`
	port_target string `data:"27017"`
	conf_source string `data:"./var/conf/mongo/"`
	conf_target string `data:"/data/config/"`
	data_source string `data:"./var/data/mongo/"`
	data_target string `data:"/data/db/"`
	list_prefix string `data:"mongo"`

	list string `name:"list CONTAINER_ID auto create" help:"数据库"`
}

func init() { ice.CodeModCmd(mongo{}) }
