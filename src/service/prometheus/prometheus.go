package prometheus

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type prometheus struct {
	client.Container
	port_source string `data:"9090"`
	port_target string `data:"9090"`
	conf_source string `data:"./var/conf/prometheus/prometheus.yml"`
	conf_target string `data:"/etc/prometheus/prometheus.yml"`
	data_source string `data:"./var/data/prometheus/"`
	data_target string `data:"/prometheus/"`
	list_prefix string `data:"prom/prometheus"`
	host_domain string `data:"https://metrics.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"数据采集"`
}

func init() { ice.CodeModCmd(prometheus{}) }
