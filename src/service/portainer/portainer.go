package portainer

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type portainer struct {
	client.Container
	port_source string `data:"8083"`
	port_target string `data:"9000"`
	conf_source string `data:"./var/conf/portainer/"`
	conf_target string `data:"/data/"`
	data_source string `data:"./var/data/portainer/"`
	data_target string `data:"/data/"`
	list_prefix string `data:"portainer/portainer"`
	host_domain string `data:"https://portainer.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"容器管理"`
}

func init() { ice.CodeModCmd(portainer{}) }
