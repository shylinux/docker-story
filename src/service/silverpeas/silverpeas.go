package silverpeas

import (
	"shylinux.com/x/ice"

	"shylinux.com/x/docker-story/src/client"
)

type silverpeas struct {
	client.Container
	port_source string `data:"8007"`
	port_target string `data:"8000"`
	conf_source string `data:"./var/conf/silverpeas/"`
	conf_target string `data:"/silverpeas/config/"`
	data_source string `data:"./var/data/silverpeas/"`
	data_target string `data:"/silverpeas/data/"`
	list_prefix string `data:"silverpeas"`

	list string `name:"list CONTAINER_ID auto create" help:"社区论坛"`
}

func init() { ice.CodeModCmd(silverpeas{}) }
