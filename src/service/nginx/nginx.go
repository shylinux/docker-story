package nginx

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type nginx struct {
	client.Container
	port_source string `data:"80"`
	port_target string `data:"80"`
	conf_source string `data:"./var/conf/nginx/"`
	conf_target string `data:"/etc/nginx/conf.d/"`
	data_source string `data:"./var/data/nginx/"`
	data_target string `data:"/usr/share/nginx/html/"`
	list_prefix string `data:"nginx"`

	list string `name:"list CONTAINER_ID auto create" help:"反向代理"`
}

func init() { ice.CodeModCmd(nginx{}) }
