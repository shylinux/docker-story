package mariadb

import (
	"shylinux.com/x/ice"
	"shylinux.com/x/icebergs/base/aaa"
	"shylinux.com/x/icebergs/base/ctx"

	"shylinux.com/x/docker-story/src/client"
)

type mariadb struct {
	client.Container
	port_source string `data:"3306"`
	port_target string `data:"3306"`
	conf_source string `data:"./var/conf/mariadb/"`
	conf_target string `data:"/etc/mysql/conf.d/"`
	data_source string `data:"./var/data/mariadb/"`
	data_target string `data:"/var/lib/mysql/"`
	list_prefix string `data:"mariadb"`

	list string `name:"list CONTAINER_ID auto create" help:"容器服务"`
}

func (s mariadb) Create(m *ice.Message, arg ...string) {
	m.Optionv(ctx.OPTS, []string{"-e", "MYSQL_ROOT_PASSWORD=" + m.Option(aaa.PASSWORD)})
	s.Container.Create(m, arg...)
}

func init() { ice.CodeModCmd(mariadb{}) }
