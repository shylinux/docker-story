package grafana

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type grafana struct {
	client.Container
	port_source string `data:"3000"`
	port_target string `data:"3000"`
	conf_source string `data:"./var/conf/grafana/grafana.ini"`
	conf_target string `data:"/etc/grafana/grafana.ini"`
	data_source string `data:"./var/data/grafana/"`
	data_target string `data:"/var/lib/grafana/"`
	list_prefix string `data:"grafana/grafana"`
	host_domain string `data:"https://monitor.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"服务监控"`
}

func init() { ice.CodeModCmd(grafana{}) }
