package onlyoffice

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type onlyoffice struct {
	client.Container
	port_source string `data:"8003"`
	port_target string `data:"80"`
	conf_source string `data:"./var/conf/onlyoffice/"`
	conf_target string `data:"/home/onlyoffice/"`
	data_source string `data:"./var/data/onlyoffice/"`
	data_target string `data:"/home/onlyoffice/"`
	list_prefix string `data:"onlyoffice/documentserver"`
	host_domain string `data:"https://office.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"办公套件"`
}

func init() { ice.CodeModCmd(onlyoffice{}) }
