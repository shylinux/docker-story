package nextcloud

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type nextcloud struct {
	client.Container
	port_source string `data:"8002"`
	port_target string `data:"80"`
	conf_source string `data:"./var/conf/nextcloud/"`
	conf_target string `data:"/var/www/html/config/"`
	data_source string `data:"./var/data/nextcloud/"`
	data_target string `data:"/var/www/html/data/"`
	list_prefix string `data:"nextcloud"`
	host_domain string `data:"https://disk.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"网盘"`
}

func init() { ice.CodeModCmd(nextcloud{}) }
