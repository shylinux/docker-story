package jenkins

import (
	"shylinux.com/x/docker-story/src/client"
	"shylinux.com/x/ice"
)

type jenkins struct {
	client.Container
	port_source string `data:"8082"`
	port_target string `data:"8080"`
	conf_source string `data:"./var/conf/jenkins/config.xml"`
	conf_target string `data:"/var/jenkins_home/config.xml"`
	data_source string `data:"./var/data/jenkins/"`
	data_target string `data:"/var/lib/jenkins"`
	list_prefix string `data:"jenkinsci/blueocean"`
	host_domain string `data:"https://deploy.shylinux.com"`

	list string `name:"list CONTAINER_ID auto create" help:"流水线"`
}

func init() { ice.CodeModCmd(jenkins{}) }
